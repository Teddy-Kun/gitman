default_target: build

build:
	cargo fetch --locked
	RUSTFLAGS="${RUSTFLAGS} -C link-arg=-lssh2" cargo b --frozen --release --features="version_check"
	
clean:
	rm -r target/

install:
	mkdir -p "${pkgdir}/usr/share/guppy"
ifeq ($(origin CARGO_TARGET_DIR),undefined)
	install -Dm755 target/release/guppy -t "/usr/bin/"
else
	install -Dm755 ${CARGO_TARGET_DIR}/release/guppy -t "/usr/bin/"
endif
	install -Dm755 resources/default_make.guppy -t "/usr/share/guppy/"

uninstall:
	rm /usr/bin/guppy
	rm -r /usr/share/guppy/
