## Description

Are you tired of manually controlling the version of programms not found in your distros repositories? This tool manages those for you. The goal is to make installing things from source more similar to installing things using a package manager. "guppy": your cute git update checker.


## Dependencies:

Debian/Ubuntu and derivatives

```
sudo apt install libssl3 libssl-dev libc6 libgit2
```

Fedora

```
sudo dnf install openssl-devel libgcc glibc libgit2
```

For everyone else you will need the following:

* cargo
* libgit2
* libssl
* libcrypto
* libgcc
* libm
* libc

## Installation

If you are on Arch Linux or any derivatives I recommend installing it from the [AUR](https://aur.archlinux.org/packages/guppy) or [AUR-git](https://aur.archlinux.org/packages/guppy-git).

###### Install rustup

Use this script to install rustup (if you haven't installed rust/cargo already). For some of you rust may be in your Repositories. You can use that as well. If it does not work please open an issue and use rustup in the meantime.
Follow the on screen instructions.

```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

###### Install guppy from source
```
git clone https://gitlab.com/Teddy-Kun/guppy/
cd guppy
make
sudo make install
```

Note: If you build guppy using ```make``` it will automatically check if an update for itself is released. You can disable the feature in the config file or you can build guppy without that feature entirely. To do the latter instead of ```make``` use ```cargo b --release```.

## Usage

First clone a repository using one of the following 2 methods:

1. Cloning the main branch:

   ```guppy -C [URL]```
   
**OR**

2. Cloning a release (tag):

   For the latest tag created:

   ```guppy -T [URL]```

   For selecting a tag among all tags:
   
   ```guppy -I [URL]```

Then build the repository with:

 - ```guppy -m [NAME]```

If you want to update all installed "packages" use the following:
 - ```guppy -u```

Uninstallation of "packages" is impossible as guppy has no knowledge of what files changed in the process. However it is possible to tell it to stop managing a package using:

 - ```guppy -f [NAME]```

Uninstallation of the programm has to be done manually afterwards. Please refer to the original projects instructions for this process.
Unfreezing of a frozen repository is done with the same command.

You can also combine most commands, i.e:

 - ```guppy -C [URL] -m```

 - ```guppy -C [URL] -m -u```

Note that in the case of "make" a [NAME] is not necessary if used in conjunction with "release" or "clone". It will automatically build the cloned repostiory

Other commands can be read using the help menu:

 - ```guppy -h```

Output:
```
Usage: target/release/guppy [options]
Running without options aliases to 'target/release/guppy -u'

Options:
    -C, --clone [URL]   Clones a repository
    -T, --tag [URL]     Clones a repositories latest tag/release if latest is
                        set to true in the config file
    -I, --interactive [URL]
                        Clones a tag but asks which one instead of the latest
    -u, --upgrade [NAME]
                        Upgrades all repositories/The chosen repository
    -m, --make [NAME]   Build the repository based on the make.guppy file.
                        If you use this at the same time as '-C' or '-R'
                        [NAME] is not required.
    -e, --edit_make [NAME]
                        Edits the makefile for [NAME]
    -f, --freeze [NAME] guppy ignores/enables updates for [NAME]
    -v, --version       Outputs the current Version
    -h, --help          print this help menu
```

#### How does guppy know how to build the repository?

Thats the trick, it doesn't. When you use the "make" command a bash script is created and then consequently opened in your default text editor. The user is then asked to complete the file as necessary/as per the instructions of the project in question. The file is then saved for future usage.

## File Locations

The config (and make) files are located under your "XDG_CONFIG_HOME/guppy/" and the cloned repositories under your "XDG_DATA_HOME/guppy/" directories. The global default make file is located under "/usr/share/guppy/default_make.guppy" (this file does not exist if you install guppy via cargo). The latest 2 log files are saved at "XDG_STATE_HOME/guppy/".

## Uninstallation

Go into your guppy repository and use the following command:
```
sudo make uninstall
```


## Authors and acknowledgment

Currently I have made everything myself. If pull requests are opened I will agknowledge the persons here.

Owner: Paul Orzel

## License

This Project is licensed under GPL-V3-only.
