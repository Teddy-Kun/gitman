//! # guppy
//!
//! A Tool to manage your installations from Git Repositories for you

use logger::{debug, info};

// we only need warn if we check for updates for guppy
#[cfg(feature = "version_check")]
use logger::warn;

use version::version;

mod file_analyzer;
mod git;
mod global_config;
mod logger;
mod options;
mod other;
mod repo_config;
mod xdg;

// We first check if the global config file exists and is formatting correctly as we need it for the log_level
fn main() {
    // checking if all required directories exist and create them if necessary
    xdg::create_xdg_dirs();

    // check if the global default_make file exists
    other::check_default_make().expect("Error getting default make");

    // check the global config and create or fix it if necessary
    global_config::check_config();

    // start the logging
    logger::init_loggger();

    // execute the not main, main function
    run();

    // flush the BufWriter
    logger::stop_logging();
}

// This is actually what we want to do
fn run() {
    // return the version set in Cargo.toml
    let version = version!();
    info("Checking for XDG directories");

    info("Collecting args");
    let args: Vec<String> = std::env::args().collect();
    let program = args[0].clone();

    #[cfg(feature = "version_check")]
    let mut upgrade_guppy = false;

    // check if a newer version of
    #[cfg(feature = "version_check")]
    if global_config::get_config().check_version && !version.contains("-dev") {
        info("Checking for a guppy update");
        // check the gitlab API if a newer tag is available
        let updated_version = match other::check_version() {
            Ok(v) => v,
            Err(e) => {
                warn(&("Error checking for guppy update".to_string() + &e.to_string()));
                String::new()
            }
        };

        // if we get a different version
        if !updated_version.is_empty() && updated_version != version {
            info("A guppy update is available");
            println!("A newer version of guppy is available!");
            println!("Your Version: {}", version);
            println!("Latest Version: {}", updated_version);
            std::thread::sleep(std::time::Duration::new(1, 0));
            upgrade_guppy = true;
        }
    }

    if args.len() == 1 {
        info("Running without arguments");
        #[cfg(feature = "version_check")]
        let upgraded = git::upgrade_all(upgrade_guppy, program);

        #[cfg(not(feature = "version_check"))]
        let upgraded = git::upgrade_all(false, program);

        if upgraded.is_empty() {
            info("Nothing to upgrade");
            return;
        }
        for name in upgraded {
            let dbg = "Making '".to_string() + &name + "' after upgrade";
            debug(&dbg);
            git::make(name);
        }
        return;
    }

    let opts = options::options();

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => {
            println!("{}\nTry '{} -h' for help.", f, program);
            return;
        }
    };

    if matches.opt_present("h") {
        options::help(&program, opts);
        return;
    }

    if matches.opt_present("v") {
        println!("{}", version);
        return;
    }

    if matches.opt_present("l") {
        println!("WIP");
    }

    // we cans safely unwrap here as it should be handled by the crate
    // clone + make
    if matches.opt_present("C") && matches.opt_present("m") {
        let repo = match git::clone(&matches.opt_str("C").unwrap()) {
            Ok(r) => r,
            Err(_) => {
                return;
            }
        };
        git::make(repo);
        return;
    }
    // just clone
    else if matches.opt_present("C") {
        git::clone(&matches.opt_str("C").unwrap()).ok();
        return;
    }
    // release + make
    else if matches.opt_present("T") && matches.opt_present("m") {
        let repo = match git::tag(matches.opt_str("T").unwrap(), false) {
            Ok(r) => r,
            Err(_) => return,
        };
        git::make(repo);
        return;
    }
    // just release
    else if matches.opt_present("T") {
        git::tag(matches.opt_str("T").unwrap(), false).ok();
        return;
    // interactive + make
    } else if matches.opt_present("I") && matches.opt_present("m") {
        let repo = match git::tag(matches.opt_str("I").unwrap(), true) {
            Ok(r) => r,
            Err(_) => return,
        };
        git::make(repo);
        return;
    }
    // just interactive
    else if matches.opt_present("I") {
        git::tag(matches.opt_str("I").unwrap(), true).ok();
        return;
    }
    // just make
    else if matches.opt_present("m") {
        git::make(matches.opt_str("m").unwrap());
    }
    // freeze and unfreeze should only be run alone so the arguments don't confuse the user
    else if matches.opt_present("f") {
        let repo_name = matches.opt_str("f").unwrap();
        other::freeze(repo_name);
        return;
    } else if matches.opt_present("e") {
        let repo_name = matches.opt_str("e").unwrap();
        other::edit_make(&repo_name);
        return;
    }

    // upgrade
    if matches.opt_present("u") {
        info("Upgrading everything");
        #[cfg(feature = "version_check")]
        let upgraded = git::upgrade_all(upgrade_guppy, program);

        #[cfg(not(feature = "version_check"))]
        let upgraded = git::upgrade_all(false, program);

        if upgraded.is_empty() {
            info("Nothing to upgrade");
            return;
        }

        for name in upgraded {
            let dbg = "Making '".to_string() + &name + "' after upgrade";
            debug(&dbg);
            git::make(name);
        }
    }
}
