//! In this file the interactions with git it are managed

use std::{
    env, fs,
    path::Path,
    process::Command,
    sync::{Arc, Mutex},
};

use git2::{build::RepoBuilder, FetchOptions, RemoteCallbacks, Repository};
use indicatif::{ProgressBar, ProgressStyle};
use substring::Substring;

use crate::{
    file_analyzer::get_install_files,
    global_config,
    logger::{debug, error, info, trace, warn},
    other::{check_cloned, choose_version, duplicate_repo, edit_make},
    repo_config::{create_config, get_config, get_config_from_path, write_config, Config},
    xdg::{get_config_home, get_data_home},
};

pub enum NameError {
    NameError,
}

// clones a repository recursivly
pub fn clone(repo_url: &String) -> Result<String, NameError> {
    info(&("Cloning: ".to_string() + repo_url));
    let name = match get_repo_name_from_url(repo_url) {
        Ok(s) => s,
        Err(_) => {
            eprintln!("Please enter a valid url");
            error("User entered an invalid url");
            return Err(NameError::NameError);
        }
    };
    let clone_location = get_data_home() + &name;
    debug(&("Clone location: ".to_string() + &clone_location));

    // check if the repository is cloned already
    if check_cloned(&name) {
        // ask the user if the duplicate should be overwritten
        if duplicate_repo() {
            debug("Repository is duplicate");
            match fs::remove_dir_all(get_data_home() + &name) {
                Ok(_) => debug("removed duplicate repository"),
                Err(e) => {
                    warn(&("Error removing duplicate repository: ".to_string() + &e.to_string()));
                }
            }
        } else {
            println!("Aborting!");
            // if they tried -C [url] -m we try to build it anyways, probably should ask as well
            return Ok(name);
        }
    }

    // Download the repository
    let repo = download(repo_url, &clone_location);

    let submodules = match repo.submodules() {
        Ok(s) => s,
        Err(e) => {
            warn(
                &("Error getting submodules, skipping downloading submodules: ".to_string()
                    + &e.to_string()),
            );
            return Ok(name);
        }
    };
    debug("Detecting submodules");
    trace(&(submodules.len().to_string() + " submodules found"));

    // we are unwrapping options here and they should never be None
    // iterate through all submodules and download them
    if !submodules.is_empty() {
        for sub in submodules {
            let name = sub.name().unwrap();
            println!("Downloading submodule: {}", name);
            let url = sub.url().unwrap();
            let location = clone_location.clone() + "/" + sub.path().to_str().unwrap();
            download(url, &location);
        }
    }

    debug("getting latest sha");
    // get the sha of the latest commit
    // I don't think this can produce an Error
    let sha = repo
        .head()
        .unwrap()
        .peel_to_commit()
        .unwrap()
        .id()
        .to_string();
    trace(&("sha is ".to_string() + &sha));

    // create the config file to this repo
    create_config(&name, repo_url, &sha, false, true, false);

    // return the name in case the user wants to build it immediately
    Ok(name)
}

pub fn tag(repo_url: String, interactive: bool) -> Result<String, NameError> {
    debug("Cloning a tag");
    let repo_name = get_repo_name_from_url(&repo_url)?;

    // check if we have already cloned the repo
    if !check_cloned(&repo_name) {
        // clone the repo normally if we haven't
        clone(&repo_url)?;
    }

    // open the cloned repository
    let repo = match Repository::open(get_data_home() + &repo_name) {
        Ok(repo) => repo,
        Err(e) => panic!("failed to open: {}", e),
    };

    // get a list of all tags
    let tags = match repo.tag_names(Option::None) {
        Ok(t) => t,
        Err(e) => {
            warn(&("Error getting tags".to_string() + &e.to_string()));
            eprintln!("Error getting tags from the repository");
            eprintln!("Are there no tags in the repository?");
            eprintln!("Continuing with working Head");
            return Ok(repo_name);
        }
    };
    trace(&("Found ".to_string() + &tags.len().to_string() + "tags"));

    if tags.is_empty() {
        warn("No tags found.");
        eprintln!("No tags found in the repository!");
        eprintln!("Continuing with -dev version.");
        return Ok(repo_name);
    }

    let tag_name: &str;
    // if they used -T grab the latest tag
    if !interactive {
        info("Switching to latest tag");
        // shouldn't cause an error
        tag_name = tags.get(tags.len() - 1).unwrap();
        println!("Switching to latest tag: {}", tag_name);
    }
    // if they used -I let them choose one
    else {
        debug("Letting user choose tag");
        let mut tag_chooser = vec![];

        // Doing unnecessary work because I have no idea how to deal with the custom String array type
        for i in 0..tags.len() {
            // shouldn't cause an error
            tag_chooser.push(tags.get(i).unwrap());
        }
        // latest should be on top/should be [0]
        tag_chooser.reverse();

        tag_name = choose_version(tag_chooser);
        info(&("User selected '".to_string() + tag_name + "'"));
        println!("Selected Tag: {}", tag_name);
    }

    debug("Finding tag in repository");
    let tag = match repo.revparse_single(tag_name) {
        Ok(t) => t,
        Err(_) => {
            error(&("Tag [".to_string() + tag_name + "] not found in repository"));
            panic!("Tag not found in Repository");
        }
    };
    // get the tree
    let tree = match tag.peel(git2::ObjectType::Commit) {
        Ok(t) => t,
        Err(_) => {
            error("Error peeling to commit");
            panic!("error peeling to commit");
        }
    };

    if let Err(e) = repo.checkout_tree(&tree, None) {
        error(&("An error occured while checking out the tree: ".to_string() + &e.to_string()));
        panic!("Error checking out tree");
    }

    // switch head to tag
    if let Err(e) = repo.set_head(&("refs/tags/".to_owned() + tag_name)) {
        error(&("An error occured while setting the head: ".to_string() + &e.to_string()));
        panic!("Error setting head");
    }

    // edit the config accordingly
    debug("Setting config");
    create_config(&repo_name, &repo_url, tag_name, true, !interactive, false);

    //again return the name
    Ok(repo_name)
}

pub fn make(repo_name: String) {
    // check if the repo even exists
    if !check_cloned(&repo_name) {
        eprintln!(
            "The Repository named {} is not managed by guppy.",
            &repo_name
        );
        return;
    }

    // grab the config
    let mut config = get_config(&repo_name);

    // path to thethe default make file
    let def_make = get_config_home() + "default_make.guppy";
    // path to the custom make file
    let make_file = get_config_home() + &repo_name + "/make.guppy";

    // check if a custom make file exists, if not copy it to the proper path
    if !Path::new(&make_file).is_file() {
        if let Err(e) = fs::copy(def_make, &make_file) {
            warn(&("Error copying default make: ".to_string() + &e.to_string()));
        }
        edit_make(&repo_name);
    }

    // this should never
    // get the current directory
    let current_dir = match env::current_dir() {
        Ok(d) => d,
        Err(e) => {
            warn(&("Cannot retrieve current working dir: ".to_string() + &e.to_string()));
            dirs::home_dir().expect("Error retrieving users home dir")
        }
    };
    // change to the repositories dir
    env::set_current_dir(get_data_home() + &repo_name).expect("Error switching to repository dir");
    // execute the make script using the shell in the global config file
    Command::new(global_config::get_config().shell)
        .arg(get_config_home() + &repo_name + "/make.guppy")
        .status()
        .expect(&("failed to execute 'sh ".to_owned() + &repo_name + "'"));
    // switch back to the dir the user was in before
    env::set_current_dir(current_dir).expect("Error switching back to previous dir");

    // change the made flag to true after successful execution of the shell script
    config.made = true;

    get_install_files(&repo_name);

    write_config(config);
}

// upgrade all repositories that have been build previously
pub fn upgrade_all(upgrade_guppy: bool, guppy_location: String) -> Vec<String> {
    let mut updated_repos = vec![];
    let mut tomls: Vec<Config> = vec![];

    if !guppy_location.contains("target/")
        && !guppy_location.contains("/usr/bin")
        && upgrade_guppy
        && !check_cloned("guppy")
    {
        match tag("https://gitlab.com/Teddy-Kun/guppy.git".to_string(), false) {
            Ok(_) => updated_repos.push("guppy".to_string()),
            Err(_) => warn("Failed updating self"),
        }
    }

    // read all directories in the config folder
    let entries = fs::read_dir(get_config_home()).expect("Error reading config dir");
    // check if there is a toml file
    for entry in entries {
        let entry = entry.expect("Error retrieving contents of config dir");
        // I don't think any of this can error at this point in time
        if entry.file_type().unwrap().is_dir() {
            let toml_path = entry.path().to_str().unwrap().to_string() + "/config.toml";
            if Path::new(&toml_path).is_file() {
                // add toml to vector
                tomls.push(get_config_from_path(&toml_path));
            }
        }
    }

    // If nothing is cloned, return an empty vector
    if tomls.is_empty() {
        return vec![];
    }

    // I don't think any unwrap here should propduce a panic
    // iterate throug all tomls
    for mut toml in tomls {
        // if they aren't the latest or are frozen don't update them
        if toml.latest {
            let name = toml.name;
            // check if we are using a dev version or a release
            if toml.is_tag {
                let repo = Repository::open(get_data_home() + &name)
                    .expect(&("Error opening Repository named ".to_string() + &name));
                // fetch latest info
                repo.find_remote("origin")
                    .unwrap()
                    .fetch(&["main"], None, None)
                    .unwrap();
                // update the repository to the latest dev version
                repo.set_head("origin/main").unwrap();
                // get all tags
                let tags = repo.tag_names(Option::None).unwrap();
                // grab the latest one
                let latest_tag = tags.get(tags.len() - 1).unwrap();
                // only update if we aren't already on the latest
                if latest_tag != toml.version {
                    repo.set_head(
                        repo.find_reference(&("refs/tag/".to_owned() + latest_tag))
                            .unwrap()
                            .name()
                            .unwrap_or("refs/heads/master"),
                    )
                    .unwrap();
                    toml.version = latest_tag.to_string();
                    toml.made = false;
                    updated_repos.push(name.clone());
                    println!("Updated {}", &name);
                } else {
                    println!("{} is up to date", &name);
                }
            }
            // if it is a dev version
            // see above for details
            else {
                let repo = Repository::open(get_data_home() + &name).unwrap();
                repo.find_remote("origin")
                    .unwrap()
                    .fetch(&["main"], None, None)
                    .unwrap();
                let remote_sha = repo
                    .find_branch("origin/main", git2::BranchType::Remote)
                    .unwrap()
                    .get()
                    .peel_to_commit()
                    .unwrap()
                    .id()
                    .to_string();
                if remote_sha != toml.version {
                    repo.set_head("origin/main").unwrap();
                    updated_repos.push(name.clone());
                    println!("Updated {}", &name);
                }

                let repo = Repository::open(get_data_home() + &name).unwrap();
                repo.find_remote("origin")
                    .unwrap()
                    .fetch(&["main"], None, None)
                    .unwrap();
                let remote_sha = repo
                    .find_branch("origin/main", git2::BranchType::Remote)
                    .unwrap()
                    .get()
                    .peel_to_commit()
                    .unwrap()
                    .id()
                    .to_string();
                if remote_sha != toml.version {
                    repo.set_head("origin/main").unwrap();
                    updated_repos.push(name.clone());
                    println!("Updated {}", &name)
                } else {
                    println!("{} is up to date", &name)
                }
            }
        }
    }
    // return a list of all updated repos so we can rebuild them afterwards
    updated_repos
}

// turns github.com/owner/repo to repo
fn get_repo_name_from_url(repo_url: &str) -> Result<String, NameError> {
    let name: &str = if repo_url.ends_with('/') {
        let slash = match repo_url[0..repo_url.len() - 1].rfind('/') {
            Some(s) => s,
            None => {
                return Err(NameError::NameError);
            }
        };
        repo_url.substring(slash + 1, repo_url.len())
    } else {
        let slash = match repo_url[0..repo_url.len()].rfind('/') {
            Some(s) => s,
            None => {
                return Err(NameError::NameError);
            }
        };
        repo_url.substring(slash + 1, repo_url.len())
    };
    Ok(name.to_string())
}

// unwrapping here where it is used should be save
// downloads a repository
fn download(url: &str, path: &str) -> Repository {
    let clone_path = Path::new(&path);

    // create a progress bar
    // this thing is wrapped in an arc + mutex to pass it to the download thread safely and update the bar with the progress
    let pb = Arc::new(Mutex::new(ProgressBar::new(0))); // Create an Arc<Mutex<ProgressBar>> to share across threads
    pb.lock().unwrap().set_style(
        ProgressStyle::default_bar()
            .template("{spinner:.white} {wide_bar:.green/white} {bytes}/{total_bytes}")
            .unwrap(),
    );

    // the download thread calls this every time we make progress and updates the bar
    let mut callbacks = RemoteCallbacks::new();
    let pb_clone = Arc::clone(&pb); // Clone the Arc<Mutex<ProgressBar>> for use in the closure
    callbacks.transfer_progress(move |stats| {
        pb_clone
            .lock()
            .unwrap()
            .set_length(stats.total_objects() as u64); // Set the total items for the progress bar
        pb_clone
            .lock()
            .unwrap()
            .set_position(stats.received_objects() as u64); // Set the received items for the progress bar
        true
    });

    // fetch options to tell the download thread to call the bar update
    let mut fetch_opts = FetchOptions::new();
    fetch_opts.remote_callbacks(callbacks);

    // actually, finally download the repo
    let repo = RepoBuilder::new()
        .fetch_options(fetch_opts)
        .clone(url, clone_path)
        .expect(&("Error cloning repository at ".to_string() + url));

    pb.lock().unwrap().finish_and_clear(); // Finish and clear the progress bar
                                           // return the cloned repo
    repo
}
