//! In this file the interactions with the XDG directories are handled.

use std::{fs, path::Path};

// gets $XDG_CONFIG_HOME, fallback is ~/.config
pub fn get_config_home() -> String {
    dirs::config_dir().unwrap().to_str().unwrap().to_owned() + "/guppy/"
}

// gets $XDG_DATA_HOME, fallback is ~/.local/share
pub fn get_data_home() -> String {
    dirs::data_dir().unwrap().to_str().unwrap().to_owned() + "/guppy/"
}

// gets $XDG_STATE_HOME, fallback is ~/.local/state
pub fn get_state_home() -> String {
    dirs::state_dir().unwrap().to_str().unwrap().to_owned() + "/guppy/"
}

pub fn create_xdg_dirs() {
    // create config folder and copy the default_make to it if they are missing
    let config_home = get_config_home();
    if !Path::new(&config_home).is_dir() {
        fs::create_dir(&config_home).expect("Failed to create config home");
    }
    let dm = config_home + "default_make.guppy";
    let default_make = Path::new(&dm);
    if !default_make.is_file() {
        let global_default_make = "/usr/share/guppy/default_make.guppy";
        if Path::new(global_default_make).is_file() {
            fs::copy("/usr/share/guppy/default_make.guppy", dm)
                .expect("Failed to copy default_make.guppy to config home");
        }
    }

    // create the folder where the repositories will be cloned to
    let data_home = get_data_home();
    if !Path::new(&data_home).is_dir() {
        fs::create_dir(&data_home).expect("Failed to create data home");
    }

    // create the folder for the log file
    let state_home = get_state_home();
    if !Path::new(&state_home).is_dir() {
        fs::create_dir(&state_home).expect("Failure to create state_home");
    }
}
