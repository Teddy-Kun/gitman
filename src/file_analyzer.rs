use std::{fs, path::Path};
use crate::xdg::{get_config_home, get_data_home};

fn get_make_scripts(name: &str) -> Vec<String> {
    let guppy_makefile = get_config_home() + name;
    let guppy_make_string = fs::read_to_string(guppy_makefile.clone()).unwrap();
    let mut make_scripts = Vec::new();
    if guppy_make_string.contains("cp") || guppy_make_string.contains("mv") || guppy_make_string.contains("install -Dm") {
        make_scripts.push(guppy_makefile);
    }

    if guppy_make_string.contains("make") {
        let makefile = get_data_home() + name + "/Makefile";
        if Path::new(&makefile).is_file() {
            make_scripts.push(makefile);
        }
        let lmakefile = get_data_home() + name + "/makefile";
        if Path::new(&lmakefile).is_file() {
            make_scripts.push(lmakefile);
        }
    }

    make_scripts
}

pub fn get_install_files(name: &str) {
    let make_scripts = get_make_scripts(name);
    println!("{:#?}", make_scripts);
}
