//! This file manages the logger and all functions related to it.

use std::{
    fs::{self, File},
    io::{BufWriter, Write},
    panic,
    path::Path,
    sync::Mutex,
};

use chrono::Local;
use lazy_static::lazy_static;
use log::LevelFilter;

use crate::{global_config::get_config, xdg::get_state_home};

// Creating the logger as a static so multiple functions can use it.
// It will run until the Program is closed anyways, so this shouldn't cause any issues.
lazy_static! {
    static ref LOG_WRITER: Mutex<BufWriter<File>> = {
        let log_file =
            File::create(get_state_home() + "guppy" + ".log").expect("Failed to create log file");
        Mutex::new(BufWriter::new(log_file))
    };
}

// Initialize the logger and a panic hook so the BufWriter gets flushed in case of a panic.
pub fn init_loggger() {
    backup_old_log();

    let log_level = get_log_level();

    // Redirect logs to the BufWriter
    env_logger::Builder::new()
        .filter_level(log_level)
        .write_style(env_logger::WriteStyle::Auto)
        .format(move |_, record| {
            // Write log message to the BufWriter's buffer
            // nothing else can write to LOG_WRITER so unwrapping should be fine
            writeln!(*LOG_WRITER.lock().unwrap(), "{}", record.args())
        })
        .try_init()
        .expect("Failed to initialize logger");

    // Set a panic hook
    panic::set_hook(Box::new(|_panic_info| {
        // Attempt to flush the BufWriter before the program terminates
        if let Ok(mut log_writer) = LOG_WRITER.lock() {
            if let Err(err) = log_writer.flush() {
                eprintln!("Failed to flush log writer before panic: {}", err);
                eprintln!("The log file is probably incomplete!")
            }
        }
        // The default panic handler will be automatically called by the runtime
    }));

    debug("Log File created successfully");
    trace(&("At ".to_string() + &get_state_home() + "guppy.log"));
}

// Flushing the log file to make sure nothing is being kept in the buffer before the memory gets freed
pub fn stop_logging() {
    info("Closing Application");
    if let Ok(mut log_writer) = LOG_WRITER.lock() {
        if let Err(err) = log_writer.flush() {
            eprintln!("Failed to flush log writer: {}", err);
        }
    }
}

// Yeah I know it currently does not get logged because it is called before the logger has been established.
// Will fix it at some point

fn backup_old_log() {
    let log_file = get_state_home() + "guppy.log";
    let old_log_file = log_file.clone() + ".old";
    if Path::new(&log_file).is_file() {
        if Path::new(&old_log_file).is_file() {
            let rm = fs::remove_file(&old_log_file);
            match rm {
                Ok(_) => info("Removed old log file"),
                Err(_) => warn("Error removing old log file"),
            }
        }
        let cp = fs::copy(&log_file, &old_log_file);
        match cp {
            Ok(_) => info("Backed up old log"),
            Err(_) => warn("Couldn't back up old log file"),
        }
    }
}

// These are here to easily get the current timestamp in the log file.
// I am allowing dead_code here as even if they are dead now they will probably be used soon

#[allow(dead_code)]
pub fn error(text: &str) {
    let timestamp = Local::now().format("%H:%M:%S");
    log::error!("{} [ERROR]: {}", timestamp, text)
}

#[allow(dead_code)]
pub fn warn(text: &str) {
    let timestamp = Local::now().format("%H:%M:%S");
    log::warn!("{} [WARN]: {}", timestamp, text)
}

#[allow(dead_code)]
pub fn info(text: &str) {
    let timestamp = Local::now().format("%H:%M:%S");
    log::info!("{} [INFO]: {}", timestamp, text)
}

#[allow(dead_code)]
pub fn debug(text: &str) {
    let timestamp = Local::now().format("%H:%M:%S");
    log::debug!("{} [DEBUG]: {}", timestamp, text)
}

#[allow(dead_code)]
pub fn trace(text: &str) {
    let timestamp = Local::now().format("%H:%M:%S");
    log::trace!("{} [TRACE]: {}", timestamp, text)
}

// Grab the log_level from the config
fn get_log_level() -> LevelFilter {
    let log_level = get_config().log_level;
    match log_level {
        0 => LevelFilter::Off,
        1 => LevelFilter::Error,
        2 => LevelFilter::Warn,
        3 => LevelFilter::Info,
        4 => LevelFilter::Debug,
        5 => LevelFilter::Trace,
        _ => {
            eprintln!("Invalid 'log_level: {}' detected!", log_level);
            eprintln!("Defaulting to 3: INFO.");
            LevelFilter::Info
        }
    }
}
