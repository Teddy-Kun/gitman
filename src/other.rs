use std::{env, fs, io, path::Path, process::Command};

use crate::{
    logger::{error, warn},
    repo_config,
    xdg::{get_config_home, get_data_home},
};
#[cfg(feature = "version_check")]
use substring::Substring;

// freezes updates for a repository
pub fn freeze(name: String) {
    let mut config = repo_config::get_config(&name);
    // repositories with latest = false will not recieve updates
    config.latest = !config.latest;
    if config.latest {
        println!("Updates for {} have been unfrozen.", name);
    } else {
        println!("Updates for {} have been frozen.", name);
    }
    repo_config::write_config(config);
}

// print available versions
fn print_versions(versions: Vec<&str>, all: bool) {
    let mut versions = versions;
    versions.reverse();
    if versions.len() > 10 && !all {
        let mut number = 10;
        for v in versions.iter().skip(versions.len() - 11) {
            println!("[{}]: {}", number, v);
            number -= 1;
        }
    } else {
        let mut i = versions.len() - 1;
        for v in versions {
            println!("[{}]: {}", i, v);
            i -= 1;
        }
    }
}

// version selector
pub fn choose_version(versions: Vec<&str>) -> &str {
    println!("Please select which version to choose:");
    println!("Show latest 10: s");
    println!("Show all: a");
    println!("Latest: 0 or l");
    print_versions(versions.clone(), false);
    loop {
        let mut choice = String::new();
        io::stdin()
            .read_line(&mut choice)
            .expect("Failed to read user input.");

        choice = choice.trim().to_lowercase();

        if choice.as_str() == "l" {
            return versions[0];
        }

        let choice_usize = choice.parse::<usize>();

        if let Ok(choice_usize) = choice_usize {
            if choice_usize < versions.len() {
                return versions[choice_usize];
            }
        }

        if choice.as_str() == "s" {
            println!(" ");
            print_versions(versions.clone(), false);
        } else if choice.as_str() == "a" {
            println!(" ");
            print_versions(versions.clone(), true);
        } else {
            println!(" ");
            println!("Invalid Input")
        }
    }
}

// if a repo already exists, ask the user for permission to overwrite
pub fn duplicate_repo() -> bool {
    println!("It appears you have already cloned this repo. Do you want to override it? (y/N)");
    let mut yn = String::new();
    io::stdin()
        .read_line(&mut yn)
        .expect("Failed to read user input.");

    yn = yn.trim().to_lowercase();

    if matches!(yn.as_str(), "y" | "ye" | "yes" | "true" | "1") {
        return true;
    }

    false
}

// check if a repository is already cloned
pub fn check_cloned(repo_name: &str) -> bool {
    let data_str = get_data_home();
    let data = Path::new(&data_str);
    if data.is_dir() {
        for dir in fs::read_dir(data).expect("Error reading cloned repository dir") {
            if dir
                .expect("Error getting folder in cloned repository")
                .file_name()
                .to_str()
                .expect("Errror getting name of dir in repository dir")
                == repo_name
            {
                return true;
            }
        }
    }
    false
}

pub fn edit_make(repo_name: &str) {
    if !Path::new(&(get_config_home() + repo_name + "/config.toml")).is_file() {
        println!(
            "The repository named {} is not managed by guppy!",
            repo_name
        );
        return;
    }

    let make_file = get_config_home() + repo_name + "/make.guppy";

    if !Path::new(&make_file).is_file() {
        if let Err(e) = fs::copy(get_config_home() + "default_make.guppy", &make_file) {
            error(
                &("Error copying default make file for repository ".to_string()
                    + repo_name
                    + ": "
                    + &e.to_string()),
            );
            eprintln!(
                "Error copying default make file for repository {}",
                repo_name
            );
            return;
        }
    }

    // Grab the default editor
    let editor = match env::var("EDITOR") {
        Ok(s) => s,
        Err(_) => {
            // Default to nano
            warn("Error getting default editor");
            "nano".to_string()
        }
    };
    // edit the make file if it didn't exist before
    Command::new(&editor)
        .arg(make_file)
        .status()
        .expect(&("Failed to execute ".to_owned() + &editor));
}

// This checks if a default_make exists anywhere where it should and downloads it if it doesn't
#[tokio::main]
pub async fn check_default_make() -> Result<(), reqwest::Error> {
    let default_make_path = "/usr/share/guppy/default_make.guppy";
    let user_make_path = get_config_home() + "/default_make.guppy";
    if !Path::new(&default_make_path).is_file() && !Path::new(&user_make_path).is_file() {
        warn("Both default_make files are missing");
        let url_default_make =
            "https://gitlab.com/Teddy-Kun/guppy/-/raw/main/resources/default_make.guppy";

        let client = reqwest::Client::builder()
            .user_agent("guppy/".to_string() + version::version!())
            .build()?;
        let response = client.get(url_default_make).send().await?;
        let body = response.text().await?;

        fs::write(user_make_path, body).expect("Failed to write default_make to user config");
    }
    Ok(())
}

// This compares the current version of guppy with the latest available on Gitlab
// Can be disabled in case people want to package it
#[cfg(feature = "version_check")]
#[tokio::main]
pub async fn check_version() -> Result<String, reqwest::Error> {
    let gitlab_api = "https://gitlab.com/api/v4/projects/Teddy-Kun%2Fguppy/releases";

    let client = reqwest::Client::builder()
        .user_agent("guppy/".to_string() + version::version!())
        .build()?;
    let response = client.get(gitlab_api).send().await?;
    let body = response.text().await?;

    let json_response = json::parse(&body).expect("Failure reading gitlab api response as json");

    let latest = json_response[0]["tag_name"].as_str().unwrap();
    let formatted_latest = latest.substring(1, latest.len()).to_string();

    Ok(formatted_latest)
}
