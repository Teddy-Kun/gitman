//! In this file the config file for gitman is managed. The options here are relevant to the application overall instead just for the repositories.

use std::{
    fs::{self, File},
    io::Read,
    path::Path,
};

use serde::{Deserialize, Serialize};
use toml::Value;

use crate::{
    logger::{debug, error, info, trace, warn},
    xdg::get_config_home,
};

#[derive(Serialize, Deserialize)]
pub struct GlobalConfig {
    // what shell the make.guppy should be executed with
    pub shell: String,

    // 0 = off
    // 1 = errors
    // 2 = warnings
    // 3 = info
    // 4 = debug
    // 5 = trace
    pub log_level: i64,
    pub check_version: bool,
}

impl Default for GlobalConfig {
    fn default() -> Self {
        Self {
            shell: "bash".to_string(),
            log_level: 3,
            check_version: true,
        }
    }
}

// the rest in this file is pretty much identical in function to config.rs refer to that if any questions arise
// should be pretty self explanatory

pub fn create_config(bash: String, log_level: i64, check_version: bool) {
    write_config(set_config(bash, log_level, check_version));
}

pub fn write_config(config: GlobalConfig) {
    let config_home = get_config_home();
    let config_file = config_home.clone() + "/config.toml";

    if !Path::new(&config_home).is_dir() {
        match fs::create_dir(&config_home) {
            Ok(_) => trace("Created config dir"),
            Err(e) => warn(&("Couldn't create config dir: ".to_string() + &e.to_string())),
        };
    }

    if Path::new(&config_file).is_file() {
        match fs::remove_file(&config_file) {
            Ok(_) => trace("Deleted global config file"),
            Err(e) => warn(&("Couldn't delete global config file ".to_string() + &e.to_string())),
        }
    }

    let conf = match toml::to_string_pretty(&config) {
        Ok(c) => c,
        Err(e) => {
            error(&("Error creating toml string: ".to_string() + &e.to_string()));
            eprintln!("An error occurd while creating the global config file please report this");
            toml::to_string(&GlobalConfig::default()).unwrap()
        }
    };
    match fs::write(&config_file, conf) {
        Ok(_) => {}
        Err(e) => {
            warn(&("Couldn't write config file to disk: ".to_string() + &e.to_string()));
            eprintln!("Couldn't write config file to disk: {}", e);
            eprintln!("Check if you have write access in your XDG_CONFIG_HOME");
            eprintln!("Assuming the default value for everything");
        }
    };
}

pub fn set_config(shell: String, log_level: i64, check_version: bool) -> GlobalConfig {
    GlobalConfig {
        shell,
        log_level,
        check_version,
    }
}

pub fn get_config() -> GlobalConfig {
    let config_file = match fs::read_to_string(get_config_home() + "config.toml") {
        Ok(c) => c,
        Err(e) => {
            warn(&("Error reading global config file: ".to_string() + &e.to_string()));
            eprintln!("Error reading global config file");
            eprintln!("Assuming default values");
            // This should never error so I am unwrapping
            toml::to_string(&GlobalConfig::default()).unwrap()
        }
    };
    let config: GlobalConfig = match toml::from_str(&config_file) {
        Ok(c) => c,
        Err(e) => {
            warn(&("Error converting config string to toml object: ".to_string() + &e.to_string()));
            eprintln!("Error converting config string to toml object: ");
            eprintln!("Assuming default values");
            GlobalConfig::default()
        }
    };
    config
}

pub fn check_config() {
    let config_home = get_config_home();
    let config_file = config_home.clone() + "/config.toml";

    if !Path::new(&config_home).is_dir() {
        match fs::create_dir(&config_home) {
            Ok(_) => trace("Creating config dir"),
            Err(e) => warn(&("Error creating config_home: ".to_string() + &e.to_string())),
        };
    }

    if !Path::new(&config_file).is_file() {
        create_config(
            GlobalConfig::default().shell,
            GlobalConfig::default().log_level,
            GlobalConfig::default().check_version,
        );
    } else {
        update_with_default();
    }
}

// if a user deletes a key-value pair from the config we add it again
fn update_with_default() {
    info("Checking global config file for correctness");
    let config_path = get_config_home() + "config.toml";
    let mut file = match File::open(&config_path) {
        Ok(f) => f,
        Err(_) => {
            if Path::new(&config_path).is_file() {
                eprint!("Your global config file is corrupt deleting it");
                match fs::remove_file(&config_path) {
                    Ok(_) => debug("Deleted corrupt config_file"),
                    Err(e) => {
                        error(&("Error deleting config file".to_string() + &e.to_string()));
                        eprintln!("Error deleting global config file");
                        eprintln!("Manual intervention needed!");
                        panic!("Error replacing corrupt global config file");
                    }
                }
            }
            write_config(GlobalConfig::default());
            match File::open(&config_path) {
                Ok(f) => f,
                Err(e) => {
                    error(
                        &("Couldn't update config file with defaults: ".to_string()
                            + &e.to_string()),
                    );
                    eprintln!();
                    panic!("Couldn't update config file with missing defaults");
                }
            }
        }
    };
    let mut contents = String::new();
    match file.read_to_string(&mut contents) {
        Ok(_) => trace("Read global config file to string"),
        Err(e) => {
            warn(&("Error converting global config file to string: ".to_string() + &e.to_string()));
            eprintln!("An error occurd reading your corrupt global config file");
            contents = toml::to_string(&GlobalConfig::default()).unwrap();
        }
    };

    let mut toml_value = match contents.parse::<Value>() {
        Ok(v) => v,
        Err(e) => {
            eprint!("Error converting string into toml, replacing everything with default values");
            warn(&("Error converting string to toml: ".to_string() + &e.to_string()));
            let v: Value = toml::to_string(&GlobalConfig::default()).unwrap().into();
            v
        }
    };

    let default = GlobalConfig::default();
    let keys = ["shell", "log_level", "check_version"];

    for k in keys {
        if toml_value.get(k).is_none() {
            match k {
                "shell" => {
                    toml_value
                        .as_table_mut()
                        .expect("Failed to add missing 'shell' key to config")
                        .entry(k)
                        .or_insert(Value::String(default.shell.clone()));
                }

                "log_level" => {
                    toml_value
                        .as_table_mut()
                        .expect("Failed to add missing 'log_level' key to config")
                        .entry(k)
                        .or_insert(Value::Integer(default.log_level));
                }

                "check_version" => {
                    toml_value
                        .as_table_mut()
                        .expect("Failed to add missing 'check_version' key to config")
                        .entry(k)
                        .or_insert(Value::Boolean(default.check_version));
                }

                _ => {
                    panic!("Cosmic Ray detected?")
                }
            }
        }
    }

    let toml_string = match toml::to_string_pretty(&toml_value) {
        Ok(s) => s,
        Err(e) => {
            warn(
                &("Error converting fixed global config to string: ".to_string() + &e.to_string()),
            );
            eprintln!("An error occured while converting the fixed config to a string");
            eprintln!("Replacing everything with the default value");
            toml::to_string(&GlobalConfig::default()).unwrap()
        }
    };
    match fs::write(get_config_home() + "config.toml", toml_string) {
        Ok(_) => {}
        Err(e) => {
            warn(
                &("An error occured while writing the fixed toml to the disk: ".to_string()
                    + &e.to_string()),
            );
            eprintln!("An error occured while writing the fixed toml to the disk");
        }
    }
}
