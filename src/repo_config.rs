//! In this file the config for the cloned repositories and all functions related to it are managed

use std::{fs, path::Path};

use serde::{Deserialize, Serialize};
use toml::Value;

use crate::{
    logger::{debug, error, info, trace, warn},
    xdg::get_config_home,
};

// the only things strictly necessary are latest and made
// the rest is for curious users and to make my life easier
#[derive(Serialize, Deserialize)]
pub struct Config {
    pub name: String,
    pub url: String,
    pub version: String,
    pub is_tag: bool,
    pub latest: bool,
    pub made: bool,
    pub installed_files: Vec<String>,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            name: "".to_string(),
            url: "".to_string(),
            version: "".to_string(),
            is_tag: false,
            latest: true,
            made: false,
            installed_files: Vec::new(),
        }
    }
}

// creates a new config and immediately writes it to disk
pub fn create_config(name: &str, url: &str, version: &str, is_tag: bool, latest: bool, made: bool) {
    write_config(set_config(
        name,
        url,
        version,
        is_tag,
        latest,
        made,
        Vec::new(),
    ));
}

// writes a config file to disk
pub fn write_config(config: Config) {
    let config_home = get_config_home() + &config.name + "/";
    let config_path = config_home.clone() + "config.toml";

    if !Path::new(&config_home).is_dir() {
        debug("Creating config home dir");
        match fs::create_dir(&config_home) {
            Ok(_) => trace("Successfully created dir"),
            Err(e) => warn(&("Couldn't create config dir: ".to_string() + &e.to_string())),
        };
    }

    // remove old config file
    if Path::new(&config_path).is_file() {
        info("removing old config file");
        match fs::remove_file(&config_path) {
            Ok(_) => trace("Removed old config file"),
            Err(e) => {
                warn(&("can't remove old config file: ".to_string() + &e.to_string()));
            }
        };
    }

    let conf = match toml::to_string(&config) {
        Ok(c) => {
            trace("successfully converted toml to string");
            c
        }
        Err(e) => {
            error(&("Couldn't convert toml to string: ".to_string() + &e.to_string()));
            panic!("Couldn't convert toml to string");
        }
    };

    match fs::write(get_config_home() + &config.name + "/" + "config.toml", conf) {
        Ok(_) => trace("toml file written to disk"),
        Err(e) => {
            eprintln!("Coudln't write the repositories config file to disk");
            eprintln!("Check if you have write access in your XDG_CONFIG_HOME");
            eprintln!("You won't be able to get updates for this repository");
            error(&("Couldn't write toml to disk: ".to_string() + &e.to_string()));
        }
    };
}

// returns a config from the inputs
pub fn set_config(
    name: &str,
    url: &str,
    version: &str,
    is_tag: bool,
    latest: bool,
    made: bool,
    installed_files: Vec<String>,
) -> Config {
    Config {
        name: name.to_string(),
        url: url.to_string(),
        version: version.to_string(),
        is_tag: is_tag.to_owned(),
        latest: latest.to_owned(),
        made: made.to_owned(),
        installed_files,
    }
}

// get the config from a local repositories name
pub fn get_config(name: &String) -> Config {
    let config_file = match fs::read_to_string(get_config_home() + name + "/" + "config.toml") {
        Ok(c) => {
            trace("read config file from name");
            c
        }
        Err(e) => {
            error(&("failed to read config file:".to_string() + &e.to_string()));
            panic!("{}", &e);
        }
    };
    let config: Config = match toml::from_str(&(config_file)) {
        Ok(c) => c,
        Err(e) => {
            error(&("failed to read string as toml file: ".to_string() + &e.to_string()));
            panic!("failed to read String as toml file")
        }
    };
    config
}

// get a config file at a path
pub fn get_config_from_path(path: &String) -> Config {
    let config_file = match fs::read_to_string(path) {
        Ok(c) => {
            trace("read config file from path");
            c
        }
        Err(e) => {
            error(&("Failed to read config file from path".to_string() + &e.to_string()));
            panic!("Failed to read config file from path");
        }
    };
    update_with_default(path);
    let config: Config = match toml::from_str(&(config_file)) {
        Ok(c) => c,
        Err(e) => {
            eprintln!(
                "The config file of your repository is corrupt please delete the file at: {}",
                path
            );
            error(&("Error reading toml file: ".to_string() + &e.to_string()));
            panic!("Error reading toml file");
        }
    };
    config
}

fn extract_missing_keywords(config_contents: &str) -> Vec<String> {
    let de = toml::de::Deserializer::new(config_contents);
    let mut old_toml = Value::deserialize(de).unwrap();

    let existing_keywords = old_toml.as_table_mut();
    let mut all_keywords = Vec::new();

    let d = Config::default();
    let t = toml::Table::try_from(d).unwrap();

    for k in toml::Table::keys(&t) {
        all_keywords.push(k.to_owned());
    }

    let missing_keywords: Vec<String> = all_keywords
        .into_iter()
        .filter(|keyword| {
            existing_keywords
                .as_ref()
                .map_or(true, |table| !table.contains_key(*&keyword))
        })
        .collect();
    missing_keywords
}

fn update_with_default(config_path: &str) {
    let mut file_content = match fs::read_to_string(config_path) {
        Ok(s) => s,
        Err(e) => {
            eprintln!("Cannot read config file at {}: {}", config_path, e);
            return;
        }
    };

    let missing_keywords = extract_missing_keywords(&file_content);

    if missing_keywords.len() > 0 {
        warn(&("Found ".to_string() + &missing_keywords.len().to_string() + " missing keywords. Attempting to fix"));
        let mut appendage = "".to_string();
        // TODO: add some keywords more intelligently. I.E. if the name is missing extract it from the path
        for k in missing_keywords {
            match k.as_str() {
                "name" => {
                    appendage += &("name = ".to_string() + "\n");
                }
                "url" => {
                    appendage += &("url = \"\"\n");
                }
                "version" => {
                    appendage += &("version = \"\"\n");
                }
                "is_tag" => {
                    appendage += &("is_tag = false\n");
                }
                "latest" => {
                    appendage += &("latest = true\n");
                }
                "made" => {
                    appendage += &("made = false\n");
                }
                "installed_files" => {
                    appendage += &("installed_files = []\n");
                }

                _ => {
                    eprintln!("Cosmic Ray detected!");
                }
            }
        }
        file_content += &appendage;
        let config: Config = match toml::from_str(&(file_content)) {
            Ok(c) => c,
            Err(e) => {
                error(&("failed to read string as toml file: ".to_string() + &e.to_string()));
                panic!("failed to read String as toml file")
            }
        };
        write_config(config);
    }
}
