//! In this file we manage what command line arguments are used. What exactly they do is found in main.rs

use getopts::Options;

// utilize the options crate for easier command line syntax
pub fn options() -> Options {
    let mut opts = Options::new();
    opts.optopt("C", "clone", "Clones a repository", "[URL]");
    opts.optopt(
        "T",
        "tag",
        "Clones a repositories latest tag/release if latest is set to true in the config file",
        "[URL]",
    );
    opts.optopt(
        "I",
        "interactive",
        "Clones a tag but asks which one instead of the latest",
        "[URL]",
    );
    opts.optflagopt(
        "u",
        "upgrade",
        "Upgrades all repositories/The chosen repository",
        "NAME",
    );
    opts.optflagopt("m", "make", "Build the repository based on the make.guppy file. If you use this at the same time as '-C' or '-R' [NAME] is not required.", "NAME");
    opts.optopt("e", "edit_make", "Edits the makefile for [NAME]", "[NAME]");
    opts.optopt(
        "f",
        "freeze",
        "guppy ignores/enables updates for [NAME]",
        "[NAME]",
    );
    opts.optflag("l", "list", "List all repositories managed by guppy");
    opts.optflag("v", "version", "Outputs the current Version");
    opts.optflag("h", "help", "print this help menu");

    opts
}

// help method
pub fn help(program: &str, opts: Options) {
    let brief = format!(
        "Usage: {0} [options]\nRunning without options aliases to '{0} -u'",
        program
    );
    print!("{}", opts.usage(&brief));
}
